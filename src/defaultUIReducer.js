import {updateObject} from './redux-utils';

export default function(state, action, loading, creating, updating, deleting) {
  if (loading && action.type === loading) return updateObject(state, {isLoading: action.payload});
  else if (creating && action.type === creating)
    return updateObject(state, {isCreating: action.payload});
  else if (updating && action.type === updating)
    return updateObject(state, {isUpdating: action.payload});
  else if (deleting && action.type === deleting)
    return updateObject(state, {isDeleting: action.payload});
  else return state;
}
