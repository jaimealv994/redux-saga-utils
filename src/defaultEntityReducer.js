import {addEntity, deleteEntity, loadEntities, updateEntity} from './redux-utils';

export default function(
  state,
  action,
  onLoadSuccess,
  onCreateSuccess,
  onUpdateSuccess,
  onDeleteSuccess
) {
  switch (action.type) {
    case onLoadSuccess:
      return loadEntities(action);
    case onCreateSuccess:
      return addEntity(state, action);
    case onUpdateSuccess:
      return updateEntity(state, action);
    case onDeleteSuccess:
      return deleteEntity(state, action);
    default:
      return state;
  }
}
