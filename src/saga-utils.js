import {select} from 'redux-saga/effects';

export function getToken() {
  return select(store => store.domain.auth.token);
}

export function getUserName() {
  return select(store => {
    return store.domain.auth.info ? store.domain.auth.info.username : null;
  });
}

export function action(type, payload) {
  return {type, payload};
}

export function constructBinnacle(operacion, usuario) {
  return {operacion, usuario};
}
