import {bindActionCreators} from 'redux';

export function updateObject(oldObject, newValues) {
  return Object.assign({}, oldObject, newValues);
}

export function updateItemInArray(array, itemId, updateItemCallback) {
  return array.map(item => {
    if (item.id !== itemId) {
      return item;
    }
    return updateItemCallback(item);
  });
}

export function updateEntity(state, action) {
  return updateItemInArray(state, action.payload.id, payload => {
    return updateObject(payload, {...action.payload});
  });
}

export function addEntity(state, action) {
  return state.concat(action.payload);
}

export const loadEntities = action => action.payload;

export const deleteEntity = (state, action) =>
  state.filter(entity => entity.id !== action.payload.id);

export const mapDispatchToProps = actionCreators => dispatch =>
  Object.keys(actionCreators).reduce((prev, current) => {
    prev[current] = bindActionCreators(actionCreators[current], dispatch);
    return prev;
  }, {});
