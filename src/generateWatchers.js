import {fork} from 'redux-saga/effects';

export default function(...watchers) {
  let array = [];
  watchers.forEach(watcher => {
    Object.keys(watcher).forEach(function(key) {
      array.push(fork(watcher[key]));
    });
  });
  return array;
}
